const path = require("path");

function resolve(dir) {
  return path.join(__dirname, dir);
}
module.exports = {
  
  outputDir: 'lgdp',
  chainWebpack: config => {
    config.resolve.alias
      .set("@", resolve("src"))
      .set("assets", resolve("src/assets"))
      .set("components", resolve("src/components"))
      .set("public", resolve("public"));
  },
  configureWebpack: {
 
  },
  devServer: {
    proxy: {
      "/lgbig": {
        target: "http://192.168.25.2:39001",
        changeOrigin: true,
        pathRewrite: {
          "^/lgbig": ""
        }
      },
      // "/lgbig": {
      //   target: "http://10.242.212.153:39001",
      //   changeOrigin: true,
      //   pathRewrite: {
      //     "^/lgbig": ""
      //   }
      // },
      // "/createStream": {
      //   target: "http://10.242.212.152:39001",
      //   changeOrigin: true,
      //   pathRewrite: {
      //     "^/lgbig": ""
      //   }
      // },
      // "/lgbig": {
      //   target: "http://10.242.212.153:4000",
      //   changeOrigin: true,
      //   pathRewrite: {
      //     "^/lgbig": ""
      //   }
      // },
    }
  }
};
