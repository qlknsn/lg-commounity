// import Vue from 'vue'
import store from '@/store/index'
import { disconnect } from 'echarts'

let socket = null
// const WS_BASE_URL = `ws://192.168.25.2:39007/starfish/sensor?action=video`
const WS_BASE_URL = `ws://10.242.212.153:39007/starfish/sensor?action=video`

let temp_url = URL.createObjectURL(new Blob());
let uuid = temp_url.toString(); // blob:https://xxx.com/b250d159-e1b6-4a87-9002-885d90033be3
URL.revokeObjectURL(temp_url);
if(localStorage.getItem("userId")){
    //不处理
}else{
    localStorage.setItem("userId", uuid.substr(uuid.lastIndexOf("/") + 1));
}



export function WSconnect({ num, token, tag, type, cycle }) {
    if (type == 'disconnect') {
        let registerJson = {
            "token": token,
            "type": type,
            "tag": tag,
            "num": num,
            "cycle": cycle,
            "userId":localStorage.getItem('userId'),
            "brand":"hk"
        }
        console.log(registerJson)
        if (registerJson.type) {
            socket.send(JSON.stringify(registerJson))
        }
        setTimeout(() => {
            socket.close()
        }, 1000);

    } else {
        socket = new WebSocket(WS_BASE_URL)
        socket.onopen = () => {
            let registerJson = {
                "token": token,
                "type": type,
                "tag": tag,
                "num": num,
                "cycle": cycle,
                "userId":localStorage.getItem('userId'),
                "brand":"hk"
            }
            console.log(registerJson)
            if (registerJson.type) {
                socket.send(JSON.stringify(registerJson))
            }
            setInterval(() => {
                let localUserId = localStorage.getItem('userId')
                let json = {
                    "type": "heatbeat",
                    "userId": localUserId
                }
                socket.send(JSON.stringify(json))
            }, 3000);
        }
    }

    socket.onerror = () => {
        // reconnect()
    }
    socket.onmessage = (evt) => {
        let wsdata = JSON.parse(evt.data)
        console.log(wsdata)
        if (wsdata.type == 'video' && wsdata.userId == localStorage.getItem('userId')) {
            // store.commit('GET_WS_CAMERA_LIST', wsdata.data)
            if (localStorage.getItem("cuttentCameraType") == wsdata.tag) {
                let arr = []
                wsdata.data.forEach(item=>{
                    if(item.dahuId){
                        arr.push(item.dahuId)
                    }else{
                        arr.push(item.gbId)
                    }
                    
                })
                console.log('====================================');
                console.log(arr);
                console.log('====================================');
                store.commit('GET_WS_CAMERA_LISTS', arr)
            }
        } else if (wsdata.type == 'face') {
            let json = {
                address: wsdata.data.address,
                camId: wsdata.data.deviceId,
                createTime: wsdata.data.createTime,
                id: wsdata.data.id,
                lat: '',
                lng: '',
                score: '',
                type: '',
                uid: '',
                url: wsdata.data.img2
            }

            store.commit('CHANGE_COMPARED_LIST', json)


        }
        // Vue;
    };
    socket.onclose = () => {
        // WSconnect()
    };
}