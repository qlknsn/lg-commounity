import * as esriloader from "esri-loader";

console.log(esriloader)
function load(modules) {
  const opt = {};
  if (window.dojoConfig) {
    opt.dojoConfig = window.dojoConfig;
  }

  if (window.apiRoot) {
    opt.url = window.apiRoot;
  }

  if (!esriloader.utils.Promise) {
    esriloader.utils.Promise = window['Promise'];
  }
  return esriloader.loadModules(modules, opt);
}

export default { load };