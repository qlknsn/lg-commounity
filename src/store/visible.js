const state ={
    caseHandleOvertimePopVisible:false,
    caseHandleEfficiencyPopVisible:false,
    residentConplainPopVisible:false,
    noHandleCasepPopVisible:false,
    noEpidemicPopVisible:false,
    residentManagePopVisible:false,
    isShowRecentDialog: false,
    isShowHotlineDialog: false,
    isShowDetailDialog: false,
    isShowDetailDialogdianti: false,
    isShowSearchDialog: false,
    highEventPopVisible:false,
    eventTypePopVisible:false,
    sanhuiPopVisible:false,
    taskHandleDetailPopVisible:false,
    isShowInformDialog: false,
    caseListPopVisible:false,
    isShowEditInformDialog: false,
    bayonetPopVisible:false,
    caseListPopTitle:'',
    highEventPopTitle:'',
    healthyPopCount:"",
    bigPicUrl:"",
    inAndOutPopVisible:false,
    carInAndOutPopVisible:false,
    renovationVisible:false
}
const actions = {
    
}
const mutations={
    SET_BIG_PIC_URL(state,res){
        state.bigPicUrl = res;
    },
    SET_RENOVATION(state,res){
        state.renovationVisible = res;
    },
    SET_HEALTHY_POP_COUNT(state,res){
        console.log(res);
        state.healthyPopCount = res;
    },
    SET_INANDOUT_POP(state,res){
        state.inAndOutPopVisible = res;
    },
    SET_CAR_INANDOUT_POP(state,res){
        state.carInAndOutPopVisible = res;
    },
    SET_HIGH_EVENT_POP_TITLE(state,res){
        state.highEventPopTitle = res;
    },
    SET_CASELIST_POP(state,res){
        state.caseListPopVisible = res;
    },
    SET_BAYONET_POP(state,res){
        state.bayonetPopVisible = res;
    },
    SET_SANHUI_POP(state,res){
        state.sanhuiPopVisible = res;
    },
    SET_TASKHANDLEDETAIL_POP(state,res){
        state.taskHandleDetailPopVisible = res;
        console.log(res)
    },
    SET_CASEHANDLEOVERTIME_POP(state,res){
        state.caseHandleOvertimePopVisible = res;
    },
    SET_HIGHEVENT_POP(state,res){
        state.highEventPopVisible = res;
    },
    SET_EVENTTYPE_POP(state,res){
        state.eventTypePopVisible = res;
    },
    SET_BIG_PIC_POP(state,res){
        state.bigPicPopVisible = res;
        console.log(state.bigPicPopVisible)
    },
    SET_NOHANDLECASE_POP(state,res){
        state.noHandleCasepPopVisible = res;
    },
    SET_NOEPIDEMIC_POP(state,res){
        state.noEpidemicPopVisible = res;
    },
    SET_RESIDENTMANAGE_POP(state,res){
        state.residentManagePopVisible = res;
    },
    SET_CASEHANDLEEFFICIENCY_POP(state,res){
        console.log(res)
        state.caseHandleEfficiencyPopVisible = res;
    },
    SET_CASE_LIST_POP_TITLE(state,res){
        state.caseListPopTitle = res;
    },
    CLOSE_DIALOG(state, data) {
        console.log("关闭")
        data;
        state.isShowEditInformDialog = false;
        state.isShowRecentDialog = false;
        state.isShowHotlineDialog = false;
        state.isShowDetailDialog = false;
        state.isShowDetailDialogdianti = false;
        state.isShowSearchDialog = false;
        state.isShowInformDialog = false;
    },
    RECENT_DIALOG(state, data) {
        state.isShowRecentDialog = data
    },
    HOTLINE_DIALOG(state, data) {
        state.isShowHotlineDialog = data
    },
    SEARCH_DIALOG(state, data) {
        state.isShowSearchDialog = data
    },
    DETAIL_DIALOG(state, data) {
        state.isShowDetailDialog = data
    },
    DETAIL_DIALOG_DIANTI(state, data) {
        state.isShowDetailDialogdianti = data
    },
    INFORM_DIALOG(state, data) {
        state.isShowInformDialog = data
    },
    EDIT_INFORM_DIALOG(state, data) {
        state.isShowEditInformDialog = data
    },
    EDIT_WAREHOUSE_DIALOG(state, data) {
        state.iswarehouseInfo = data
    },
}
export default{
    state,actions,mutations
}
