import {
  login,
  recentMassage,
  sendMassage,
  personList,
  getMessageList,
  getSearchList,
  getTaskDetail,
  getTootipList,
  getConfig,
  queryDistrictByParentId,
  faceRecords,
  faceCamera,
  carCamera,
  carRecords,
  cameraNum,
  getHkCameraByTagConfig,
} from "@/store/api/infoMessage/infoMessage";
import { cameraPreviewURL } from "@/store/api/cameraLoop/cameraLoop";
import store from "@/store";
import router from "@/router";
import { ElMessage } from "element-plus";
const state = {
  loginId: "",
  recentMessageObj: {},
  personList: [],
  messageList: [],
  messageObj: {},
  wenTaskDetail: {
    alarmProcesses: [],
  },
  searchListObj: {
    events: {list:[],totalPage:null},
    cameras: {list:[],totalPage:null},
    users: {list:[],totalPage:null},
    notices: {list:[],totalPage:null},
    specialPopulations: {list:[],totalPage:null},
    car:{list:[],totalPage:null}
  },
  getTootipList: {},
  getConfig: {},
  parentDistrictIdList: [],
  carCameraList: [],
  cameraNum: [
    { num: 0 },
    { num: 0 },
    { num: 0 },
    { num: 0 },
    { num: 0 },
    { num: 0 },
  ],
  getHkCameraByTag: [],
};
const actions = {
  login({ commit }, params) {
    let p = login(params);
    p.then((res) => {
      commit("LOGIN_INFO", res);
      localStorage.setItem("bigparentDistrictId",res.data.id)
    });
  },
  cameraNum({ commit }) {
    let p = cameraNum();
    p.then((res) => {
      commit("CAMERA_NUM", res.data);
    });
    return p;
  },
  cameraUrl({ commit }, params) {
    let p = cameraPreviewURL(params);
    return p;
  },
  faceRecords({ commit }, params) {
    let p = faceRecords(params);
    return p;
  },
  carRecords({ commit }, params) {
    let p = carRecords(params);
    return p;
  },
  faceCamera({ commit }, params) {
    let p = faceCamera(params);
    return p;
  },
  carCamera({ commit }, params) {
    let p = carCamera(params);
    return p;
  },
  queryDistrictByParentId({ commit }) {
    let p = queryDistrictByParentId();
    p.then((res) => {
      commit("PARENT_DISTRICTID_LIST", res.data);
    });
    return p;
  },
  getConfig({ commit }) {
    let p = getConfig();
    return p;
  },

  sendMassage({ commit }, params) {
    let p = sendMassage(params);
    p.then((res) => {
      if (res.code == 200) {
        ElMessage({
          type: "success",
          message: "发布成功",
        });
        store.dispatch("recentMassage", {});
        store.dispatch("getMessageList", {
          page: 1,
          limit: 10,
          type: null,
          timeRangeType:6
        });
      } else {
        ElMessage({
          type: "error",
          message: "发布失败",
        });
      }
    });
    store.commit("CLOSE_DIALOG", false);
  },
  recentMassage({ commit }, params) {
    let p = recentMassage(params);
    p.then((res) => {
      commit("RECENT_MESSAGE", res);
    });
  },
  personList({ commit }, params) {
    let p = personList(params);
    p.then((res) => {
      commit("PERSON_LIST", res);
    });
  },
  getMessageList({ commit }, params) {
    let p = getMessageList(params);
    p.then((res) => {
      commit("MESSAGE_LIST", res);
    });
  },
  getSearchList({ commit }, params) {
    let p = getSearchList(params);
    p.then((res) => {
      commit("GET_SEARCH_LIST", res);
    });
  },
  getTaskDetail({ commit }, params) {
    let p = getTaskDetail(params);
    p.then((res) => {
      commit("GET_TASK_DETAIL", res);
    });
  },
  getTootipList({ commit }, params) {
    let p = getTootipList(params);
    p.then((res) => {
      commit("GET_TOOTIP_LIST", res);
    });
  },
  getHkCameraByTagConfig({ commit }, params) {
    let p = getHkCameraByTagConfig(params);
    // p.then((res) => {
    //   commit("GET_TOOTIP_LIST", res);
    // });
    return p;
  },
};
const mutations = {
  CAMERA_NUM(state, res) {
    state.cameraNum = res;
  },
  GET_TOOTIP_LIST(state, res) {
    state.getTootipList = res;
  },
  LOGIN_INFO(state, res) {
    if (res.code == 200) {
      localStorage.setItem("loginId", res.data.id);
      router.push({ path: "/screen" });
    } else {
      ElMessage({
        type: "error",
        message: res.msg,
      });
      return;
    }
    localStorage.setItem("level", res.data.role.code);
    localStorage.setItem("token", res.data.token);
    localStorage.setItem("districtId", res.data.id);
    localStorage.setItem("roleId", res.data.role.id);
    if(res.data.user.parentDistrictName==null){
      localStorage.setItem("districtName", "");
    }else{
    localStorage.setItem("districtName", res.data.name);
    }
  },
  RECENT_MESSAGE(state, res) {
    state.recentMessageObj = res.data;
  },
  PERSON_LIST(state, res) {
    state.personList = res.data;
  },
  MESSAGE_LIST(state, res) {
    state.messageObj = res.data;
    state.messageList = res.data.list;
  },
  GET_SEARCH_LIST(state, res) {
    state.searchListObj = res.data;
  },
  PARENT_DISTRICTID_LIST(state, res) {
    state.parentDistrictIdList = res;
  },
  GET_TASK_DETAIL(state, res) {
    state.wenTaskDetail = res.data;
    // state.wenTaskDetail = res.data;
  },
};
export default {
  state,
  actions,
  mutations,
};
