import {
    cameraList,
    cameraPreviewURL,
    eventList,
    cameraListV1,
    getLoopCfg,
    getpreUrl,
    cameraPreviewURLV2
} from "@/store/api/cameraLoop/cameraLoop";
import store from '@/store'
const state = {
    cameralist: [{videoid:'',server:'',port:'',videoData:{id:'',rtsp:''}}],
    getcameraParams: {
        page: 1,
        limit: 1,
        tag: '',
        ai: ''
    },
    eventLists: [],
    hlscameraList:[{hls:'',cameraCode:''},{hls:'',cameraCode:''},{hls:'',cameraCode:''},{hls:'',cameraCode:''}],
    rtspcameraList:[],
    rtspcamerabaozhangList:[{cameraCode:'',url:''},{cameraCode:'',url:''},{cameraCode:'',url:''},{cameraCode:'',url:''}],
    cameraloopConfig:{},
    cameraDahualist:[],
    dahuaWs:null
}
const actions = {
    getpreUrl({ commit }, params) {
        let p = getpreUrl(params)
        return p;
    },
    getLoopCfg({ commit }, params) {
        let p = getLoopCfg(params)
        p.then(res => {
            console.log(res)
            commit('GET_LOOP_CFG', res.data)
        })
    },
    cameraListV1({ commit }, params) {
        let p = cameraListV1(params)
        p.then(res => {
            console.log(res)
            commit('GET_CAMERA_LISTV1', res.data)
        })
    },
    camerabaozhangListV1({ commit }, params) {
        let p = cameraListV1(params)
        // return p;
        p.then(res => {
            console.log(res)
            commit('GET_CAMERA_BAOZHANG_LISTV1', res.data)
        })
    },
    // cameraList({ commit }, params) {
    //     let p = cameraList(params)
    //     p.then(res => {
    //         commit('GET_CAMERA_LIST', res.data)
    //     })
    // },
    eventList({ commit }, params) {
        let p = eventList(params)
       return p;
    },
    cameraPreviewURL({ commit }, params) {
        let p = cameraPreviewURL(params)
        p.then(res => {
            commit('GET_CAMERA_PREVIEWURL', res.data)
        })
    },
    cameraPreviewURLV2({ commit }, params) {
        let p = cameraPreviewURLV2(params)
        return p;
    },
}
const mutations = {
    CAMERA_DAHUA:function(state,res) {
        state.cameraDahualist = res
    },
    GET_LOOP_CFG:function(state,res) {
        state.cameraloopConfig = res
    },
    GET_CAMERA_BAOZHANG_LISTV1(state, res){
        let port = 9001
        res.forEach(item=>{
            item.port = port++
            item.server = '10.242.212.153'
        })
        console.log(res)
        state.rtspcamerabaozhangList = res

    },
    GET_CAMERA_LISTV1(state, res){
        let port = 9001
        res.forEach(item=>{
            item.port = port++
            item.server = '10.242.212.153'
        })
        console.log(res)
        state.rtspcameraList = res

    },
    GET_CAMERA_PREVIEWURL(state, res) {
        // return res[0].url
        console.log(res)
        state.hlscameraList = res
    },
    GET_CAMERA_LIST(state, res) {
        state.eventLists = []
        let arr = []
        res.forEach(item => {
            // console.log(item)
            let par = {
                protocol: "hls", //视频格式
                streamType: 0,
                cameraCode: item.cameraCode //相机id
                // cameraCode: item.dahuId //相机id
            }
            // console.log(par)
            arr.push(par)
            // console.log(arr)
            // item.url = 
            store.dispatch('cameraPreviewURL',arr)
            if (state.getcameraParams.ai == 1) {
                store.dispatch('eventList', { deviceId: item.cameraCode }).then(res=>{
                    state.eventLists = [...state.eventLists,...res.data.list]
                    console.log(state.eventLists)
                })
                
            }
        })
        state.cameralist = res
    },
    GET_WS_CAMERA_LIST(state, res) {
        // state.eventLists = []
        // let arr = []
        // res.forEach(item => {
        //     // console.log(item)
        //     let par = {
        //         protocol: "hls", //视频格式
        //         streamType: 0,
        //         cameraCode: item //相机id
        //     }
        //     // console.log(par)
        //     arr.push(par)
        //     // console.log(arr)
        //     // item.url = 
        //     store.dispatch('cameraPreviewURL',arr)
        //     if (state.getcameraParams.ai == 1) {
        //         store.dispatch('eventList', { deviceId: item }).then(res=>{
        //             state.eventLists = [...state.eventLists,...res.data.list]
        //             console.log(state.eventLists)
        //         })       
        //     }
        // })
        let port = 9001
        res.forEach(item=>{
            item.port = port++
            item.server = '10.242.212.153'
        })
        console.log(res)
        state.rtspcameraList = res
    },
    GET_WS_CAMERA_LISTS(state,res){
        state.hlscameraList = res
    },
    CHANGE_PARAMS(state, res) {
        state.getcameraParams[res.name] = res.value
    },
    SET_WS_DATA(state, res) {
        state.dahuaWs = res
    },
};
export default {
    state,
    actions,
    mutations,
};
