import {
  getDistrictName,
  healthStatus,
  getEventCount,
  getEpidemicCount,
  getNoticeLife,
  getInOutCount,
  getEventList,
  getCameraSnap,
  eventPage,
  getCompared,
  getFireHouse,
  getElevator,
  getWarehouse,
  getcamerabzList,
  lifeUsers,
  getInOutCountDashboard,
  lifeList,
  getHkCameraByTag,
  getRenovationList,
  getLatLng,
  getJMLatLng,
  getBuilding,
  getRoom,
  getLift,
  getWarehouseList,
  getWareInfo
} from "@/store/api/lifeTizheng/lifeTizheng";

import {
  cameraPreviewURL
} from "@/store/api/cameraLoop/cameraLoop";
import store from "../..";
const state = {
  level: null,
  juweiNames: [],
  allJuwei:[],
  shequNames: [],
  locusPopVisible: false,
  showThingJsMap: true,
  healthStatusList: [],
  getEventCountList: [],
  getheighEventCountList: [],
  getEpidemicCountS: {},
  getnoticelifeS: [],
  getinoutCount: {},
  eventList: {
    list: []
  },
  selfTogetherTaskList: {
    list: []
  },
  eventListmarker: [],
  healthyParams: {
    timeRangeType: 3
  },
  eventarams: {
    timeRangeType: 1,
    type: 1,
    source: 'IOT',
    sheQuId: null,
    parentDistrictId: null
  },
  eventTypeparams: {
    timeRangeType: 1,
    type: 2,
    sheQuId: null,
    parentDistrictId: null
  },
  noticeParmas: {
    type: '',
  },
  getCameraSnapList: {},
  getCameraSnapguijiList: [{}],
  getComparedList: {},
  currentType: '',
  fireHouseList: [],
  fireHouseListVisible: false,
  elevatorList: [],
  elevatordata: {},
  elevatorListVisible: false,
  houseinfoVisible: false,
  houseUseVisible: false,
  houseInfo: {},
  zizeng: 0,
  yiqingbukongPeoPopVisible: false,
  xiaoquPeoVisible: false,
  villagePeolist: [],
  typeName: '',
  inoutCountdashboard: {},
  lifeList: [],
  typebukong: {},
  inoutParams: {
    timeRangeType: 1
  },
  carParams: {},
  cameraMarkerList: [],
  // 撒点相机信息和显示属性
  singlecameraInfo: { hls: '' },
  singlecameraVisible: false,
  getRenovationList: {},
  showSwiper: false,
  houseUseInfo: {},
  // 电梯信息
  singledianticameraInfo: { monitorurl: "" },
  // 电梯显示属性
  singlediantivisible: false,
  singlewareInfo:[],
  wareHouseId:""
}
const actions = {
  // 画区域
  getLatLng({ commit }, params) {
    let p = getLatLng(params)
    p.then(res => {

      let datacircle = {
        "type": "FeatureCollection",
        "option": "circle",
        "features": res.data.features
      }
      let a = res.data.features.filter(item => {
        return item.properties.districtId == localStorage.getItem('bigparentDistrictId')
      })
      let dataorientation = {
        "type": "FeatureCollection",
        "option": "orientation",
        "features": a
      }
      let thingmap = document.getElementById('thingmap')
      thingmap.contentWindow.postMessage(datacircle, "*")
      thingmap.contentWindow.postMessage(dataorientation, "*")
    })
  },

  // 获取仓库详情
  getWareInfo({ commit }, params) {
    let p = getWareInfo(params)
    p.then(res => {
      commit("GET_WARE_INFO",res)
    })
  },
  // 获取建模数据
  getJMLatLng({ commit }, params) {
    let p = getJMLatLng(params)
    p.then(res => {
      let data = {
        "type": "FeatureCollection",
        "option": "JM",
        "features": res.data.features
      }
      let thingmap = document.getElementById('thingmap')
      thingmap.contentWindow.postMessage(data, "*")
    })
  },
  // 获取电梯数据
  getLift_zizhi({ commit }, params) {
    let par = {
      "page": 1,
      "limit": 10000
    }
    let p = getLift(par)
    p.then(res => {
      commit('ADD_DIANTIMARKER_ZIZHI_LIST', { data: res.data.list, type: params.type })
    })
  },
  // 获取仓库列表数据
  getWarehouseList({ commit }, params) {
    let p = getWarehouseList(params)
    p.then(res => {
      commit('ADD_WAREHOUSE_LIST', res.data.list)
    })
  },
  // 获取电梯数据
  getLift({ commit }, params) {
    let par = {
      "page": 1,
      "limit": 10000
    }
    let p = getLift(par)
    p.then(res => {
      commit('ADD_DIANTIMARKER_LIST', { data: res.data.list, type: params.type })
    })
  },
  // 获取房间详情
  getRoom({ commit }, params) {
    let p = getRoom(params)
    p.then(res => {
      store.commit("CHANGE_HOUSEUSEVISIBLE", true);
      console.log('====================================');
      console.log(res);
      console.log('====================================');
      commit("HOUSE_ISER_INFO", res)
    })
  },
  // 获取楼栋详情
  getBuilding({ commit }, params) {
    let p = getBuilding(params)
    p.then(res => {
      console.log('====================================');
      console.log(res);
      console.log('====================================');
      commit('GET_HOUSE_INFO', res.data)
    })
  },
  getDistrictName({ commit }, params) {
    let p = getDistrictName(params)
    p.then(res => {
      commit('GETDISTRICTNAME', res);
    })
  },

  // 摄像头列表
  getHkCameraByTag({ commit }, params) {
    let par = { tag: params.tag }
    let p = getHkCameraByTag(par)
    p.then(res => {
      console.log(res)
      // commit('GET_CAMERAMARKER_LIST',res)
      commit('ADD_MARKER_LIST', { data: res.data, type: params.type })
    })
  },
  // 疫情布控
  lifeList({ commit }, params) {
    let p = lifeList(params)
    p.then(res => {
      console.log(res)
      commit('GET_LIFE_LIST', res)
    })
  },
  // 装修管理列表
  getRenovationList({ commit }, params) {
    let p = getRenovationList(params)
    return p;
  },
  // 进出统计
  getInOutCountDashboard({ commit }, params) {
    let p = getInOutCountDashboard(params)
    p.then(res => {
      commit('GET_INOUTCOUNT_DASHBOARD', res)
    })
  },
  // 居委人
  lifeUsers({ commit }, params) {
    let p = lifeUsers(params)
    p.then(res => {
      console.log(res);
      commit('GET_VILLAGE_PEO', res)
    })
  },
  // 视频保障
  getcamerabzList({ commit }, params) {
    let p = getcamerabzList(params)
    return p;
  },
  camerabzPreviewURL({ commit }, params) {
    let p = cameraPreviewURL(params)
    p.then(res => {
      commit('GET_CAMERAMARKER_INFO', res.data[0])
    })

  },
  // 仓库
  getWarehouse({ commit }, params) {
    let p = getWarehouse(params)
    p.then(res => {
      commit('GET_WARE_HOUSE', res.data)
    })
  },
  // 电梯
  getElevator({ commit }, params) {
    let p = getElevator(params)
    p.then(res => {
      commit('GET_ELEVATOR', res.data)
    })
  },
  // 微信消防站
  getFireHouse({ commit }, params) {
    let p = getFireHouse(params)
    p.then(res => {
      commit('GET_FIRE_HOUSE', res.data)
    })
  },
  // 健康状态一级页面
  healthStatus({ commit }, params) {
    let p = healthStatus(params)
    p.then(res => {
      commit('HEALTH_STATUS', res.data)
    })
  },
  // 事件类别一级页面
  getEventCount({ commit }, params) {
    let p = getEventCount(params)
    p.then(res => {
      if (params.type == 1) {
        let temp = res.data;
        let arr = temp.data.slice(0, 5)
        temp.data = arr;
        commit("GET_HEIGH_EVENT_COUNT", temp)
      } else {
        commit("GET_EVENTType_COUNT", res.data)
      }

    })
  },
  // 告警事件占比
  warningtask({ commit }, params) {
    let p = getEventCount(params)
    commit;
    return p;
  },
  // 告警事件详情
  warningtaskpop({ commit }, params) {
    let p = getEventCount(params)
    commit;
    return p;
  },
  // 疫情防控一级页面
  getEpidemicCount({ commit }, params) {
    let p = getEpidemicCount(params)
    p.then(res => {
      commit("GET_EPIDEMIC_COUNT", res.data)
    })
  },
  // 近期待办一级页面
  getNoticeLife({ commit }, params) {
    let p = getNoticeLife(params)
    p.then(res => {
      commit("GET_NOTICE_LIFE", res.data)
    })
  },
  // 进出统计人房信息一级页面
  getInOutCount({ commit }, params) {
    let p = getInOutCount(params)
    p.then(res => {
      commit("GET_INOUT_COUNT", res.data)
    })
  },
  // 任务列表
  getEventMarkerList({ commit }, params) {
    let p = eventPage(params);
    p.then((res) => {
      commit("GET_EVENTMARKER_LIST", res);
    });
  },
  // 任务列表
  getEventList({ commit }, params) {
    let p = getEventList(params);
    p.then((res) => {
      commit("GET_EVENT_LIST", res);
    });
    return p;
  },
  // 自治共治任务列表
  selfTogetherTaskList({ commit }, params) {
    let p = getEventList(params);
    p.then((res) => {
      commit("GET_SELFTOGETHER_TASK_LIST", res);
    });
    return p;
  },
  // 抓拍列表
  getCameraSnap({ commit }, params) {
    let p = getCameraSnap();
    p.then((res) => {
      commit("GET_CAMEAR_SNAP_LIST", res);
    });
  },
  // 抓拍轨迹列表
  getCameraSnapguiji({ commit }, params) {
    let p = getCameraSnap(params)
    p.then((res) => {
      commit("GET_CAMEAR_SNAPGUIJI_LIST", res);
    });
  },
  // 人脸对比列表
  getCompared({ commit }, params) {
    let p = getCompared();
    p.then((res) => {
      commit("GET_COMPARED_LIST", res);
    });
  },
}
const mutations = {
  HOUSE_ISER_INFO(state, res) {
    state.houseUseInfo = res.data
  },
  GET_WARE_INFO(state, res) {
    state.singlewareInfo = res.data
  },
  GETDISTRICTNAME(state, res) {
    const level = res.data.map((item) => {
      return item.level
    })
    state.level = level[0]

    var arr1 = []
    for (var i of res.data) {
      arr1 = arr1.concat(i.children)
    }
    var arr2 = []
    for (var j of arr1) {
      arr2 = arr2.concat(j.children)
    }
    if (level[0] == 3) {//镇账号
      state.shequNames = arr1.map((item) => {
        return { label: item.id, value: item.name }
      })
      let val = {
        label: null,
        value: "全部社区",
      };
      state.shequNames.push(val);
      // console.log(state.shequNames);
      // console.log(arr2);
      state.juweiNames = arr2
      state.allJuwei = arr2.map((item) => {
        if(item){
          return { label: item.id, value: item.name }
        }    
      })
      let val2 = {
        label: null,
        value: "全部居委",
      };
      state.allJuwei.push(val2);
      console.log(state.allJuwei);
    } else if (level[0] == 4) {//社区账号
      state.juweiNames = arr1.map((item) => {
        return { label: item.id, value: item.name }
      })
      let val3 = {
        label: null,
        value: "全部居委",
      };
      state.juweiNames.push(val3);
    }
  },
  GET_CAMERAMARKER_LIST(state, res) {
    // res.data.forEach(item=>{

    // })

    // state.cameraMarkerList = res.data
  },
  CHANGE_INOUT_DATA(state, res) {
    state.inoutParams.timeRangeType = res
  },
  CHANGE_TYPE(state, res) {
    state.typebukong = res
  },
  GET_LIFE_LIST(state, res) {
    state.lifeList = res.data
  },
  GET_INOUTCOUNT_DASHBOARD(state, res) {
    state.inoutCountdashboard = res.data
  },
  CHANGE_TYPENAME(state, res) {
    state.typeName = res
  },
  GET_VILLAGE_PEO(state, res) {
    res.data.list.forEach(item => {
      if (item.gender) {

      } else {
        item.gender = {
          desc: ''
        }
      }
    })
    state.villagePeolist = res.data
  },
  GET_HOUSE_INFO(state, res) {
    state.houseInfo = res
  },
  CHANGE_HOUSEINFOVISIBLE(state, res) {
    state.houseinfoVisible = res
  },
  CHANGE_HOUSEUSEVISIBLE(state, res) {
    console.log('====================================');
    console.log(res);
    console.log('====================================');
    state.houseUseVisible = res
  },
  SET_CAR_PARAMS(state, res) {
    state.carParams = res
  },
  CHANGE_YIQINGBUKONG_VISIBLE(state, res) {
    state.yiqingbukongPeoPopVisible = res
  },
  CHANGE_XIAOQUPEO_VISIBLE(state, res) {
    state.xiaoquPeoVisible = res
  },
  CHANGE_ELEVATORLISTVISIBLE(state, res) {
    state.elevatorListVisible = res
  },
  GET_ELEVATORLIST(state, res) {
    state.elevatordata = res
  },
  GET_ELEVATOR(state, res) {
    console.log(res)
    store.commit('ADD_ELEVATOR_MARKER', res.list)
  },
  CHANGE_FIRE_HOUSELISTVISIBLE(state, res) {
    state.fireHouseListVisible = res
  },
  GET_FIRE_HOUSELIST(state, res) {
    state.fireHouseList = res
  },
  GET_FIRE_HOUSE(state, res) {
    store.commit('ADD_FIREHOSUE_MARKER', res.list)
  },
  CHANGE_CURRENT_TYPE(state, res) {
    state.currentType = res
  },
  GET_CAMEAR_SNAPGUIJI_LIST(state, res) {
    console.log(res)
    state.getCameraSnapguijiList = res.data
  },
  GET_CAMEAR_SNAP_LIST(state, res) {
    state.getCameraSnapList = res
    state.getCameraSnapList.data.slice(0, 20)
  },
  GET_COMPARED_LIST(state, res) {
    state.getComparedList = res

  },
  CHANGE_COMPARED_LIST(state, res) {

    state.getCameraSnapList.data.unshift(res)
    // console.log()
    state.getCameraSnapList.data.slice(0, 20)
    // console.log(state.getCameraSnapList)
  },
  CHANGE_NOTICE_PARAMS(state, res) {
    state.noticeParmas.type = res
  },
  CHANGE_EVENT_PARAMS(state, res) {
    state.eventarams.timeRangeType = res
    state.eventTypeparams.timeRangeType = res
  },
  CHANGE_EVENT_DISTRICT_SHEQU_PARAMS(state, res) {
    state.eventarams.sheQuId = res
    state.eventTypeparams.sheQuId = res
  },
  CHANGE_EVENT_DISTRICT_JUWEI_PARAMS(state, res) {
    state.eventarams.parentDistrictId = res
    state.eventTypeparams.parentDistrictId = res
  },
  CHANGE_HEALTHY_PARAMS(state, res) {
    state.healthyParams.timeRangeType = res
  },
  GET_INOUT_COUNT(state, res) {
    state.getinoutCount = res
  },
  GET_EVENT_LIST(state, res) {
    state.eventList = res.data;
  },
  GET_EVENTMARKER_LIST(state, res) {
    // 展示所有时间的弹框
    store.commit("SET_HIGHEVENT_POP", true);
    store.commit("SET_EVENT_LIST", res.data.page)
    console.log(res);
    // 撒点数据已完成和未完成 1：未完成，2已完成
    // state.eventList = res.data;
    let arr1 = res.data.event.filter(item => item.status == 1)
    let arr2 = res.data.event.filter(item => item.status == 2)
    store.commit('ADD_MARKER', { data: arr2, type: 'resolve' })
    store.commit('ADD_MARKER', { data: arr1, type: 'unresolve' })
  },
  SET_EVENT_LIST(state, res) {
    state.eventListmarker = res;
  },
  SET_SHOWSWIPER(state, res) {
    state.showSwiper = res;
  },
  GET_NOTICE_LIFE(state, res) {
    state.getnoticelifeS = res
  },
  GET_EPIDEMIC_COUNT(state, res) {
    state.getEpidemicCountS = res
  },
  GET_EVENTType_COUNT(state, res) {
    state.getEventCountList = res.data
  },
  GET_HEIGH_EVENT_COUNT(state, res) {
    state.getheighEventCountList = res.data
  },
  HEALTH_STATUS(state, res) {
    state.healthStatusList = res
  },
  CHANGE_MAP(state) {
    state.showThingJsMap = !state.showThingJsMap
  },
  GET_SELFTOGETHER_TASK_LIST(state, res) {
    state.selfTogetherTaskList = res.data
  },
  CLOSE_LOCUS_POP(state, res) {
    state.locusPopVisible = res
  },
  CLEAR_MARKER() {
    let json_point = {
      "type": "FeatureCollection",
      "option": 'clearmarker',
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": []
    }
    let thingmap = document.getElementById('thingmap')
    thingmap.contentWindow.postMessage(json_point, "*")
  },
  CLEAR_HANDLE_MARKER() {
    let json_point = {
      "type": "FeatureCollection",
      "option": 'handle_clearmarker',
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": []
    }
    let thingmap = document.getElementById('thingmap_zizhi')
    thingmap.contentWindow.postMessage(json_point, "*")
  },
  ADD_WAREHOUSE_LIST(state, res) {
    let json_point = {
      "type": "FeatureCollection",
      "option": 'warehouse',
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": []
    }
    if (res.length > 0) {
      res.forEach(item => {
        json_point.features.push(
          {
            "type": "Feature",
            "geometry": {
              "type": "Point",
              "coordinates": [
                item.warehouse.lng,
                item.warehouse.lat
              ],
            },
            "properties": {
              "data": item.warehouse,
              "type": 'elevator',
            },
          }
        )
      })
      let thingmap = document.getElementById('thingmap')
      thingmap.contentWindow.postMessage(json_point, "*")
    }
  },
  ADD_ELEVATOR_MARKER(state, res) {
    let json_point = {
      "type": "FeatureCollection",
      "option": 'elevator',
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": []
    }
    if (res.length > 0) {
      res.forEach(item => {
        json_point.features.push(
          {
            "type": "Feature",
            "geometry": {
              "type": "Point",
              "coordinates": [
                item.lng,
                item.lat
              ],
            },
            "properties": {
              "data": item,
              "type": 'elevator',
            },
          }
        )
      })
      let thingmap = document.getElementById('thingmap')
      thingmap.contentWindow.postMessage(json_point, "*")
    }
  },
  ADD_FIREHOSUE_MARKER(state, res) {
    let json_point = {
      "type": "FeatureCollection",
      "option": 'fireHouse',
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": []
    }
    if (res.length > 0) {
      res.forEach(item => {
        json_point.features.push(
          {
            "type": "Feature",
            "geometry": {
              "type": "Point",
              "coordinates": [
                item.fireHouse.lng,
                item.fireHouse.lat
              ],
            },
            "properties": {
              "data": item.device,
              "type": 'fireHouse',
            },
          }
        )
      })
      let thingmap = document.getElementById('thingmap')
      thingmap.contentWindow.postMessage(json_point, "*")
    }
  },
  // 摄像机撒点
  ADD_MARKER_LIST(state, { data = [], type }) {
    console.log(data);
    console.log(type);
    let json_point = {
      "type": "FeatureCollection",
      "option": type,
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": [

      ]
    }
    // json_point.features
    console.log(data)
    if (data.length > 0) {
      data.forEach(item => {
        json_point.features.push({
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [
              item.lng,
              item.lat
            ],
          },
          "properties": {
            "data": item,
            "type": 'cameraType',
            "id": item.cameraCode
          },
        })
      })
    }

    let thingmap = document.getElementById('thingmap')
    thingmap.contentWindow.postMessage(json_point, "*")

  },
  // 摄电梯撒点
  ADD_DIANTIMARKER_LIST(state, { data = [], type }) {

    let json_point = {
      "type": "FeatureCollection",
      "option": type,
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": [

      ]
    }
    // json_point.features
    console.log(data)
    if (data.length > 0) {
      data.forEach(item => {
        json_point.features.push({
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [
              item.longitude,
              item.latitude
            ],
          },
          "properties": {
            "data": item,
            "type": 'cameraType',
            "id": item.id
          },
        })
      })
    }

    let thingmap = document.getElementById('thingmap')
    thingmap.contentWindow.postMessage(json_point, "*")


  },
  // 摄电梯撒点
  ADD_DIANTIMARKER_ZIZHI_LIST(state, { data = [], type }) {

    let json_point = {
      "type": "FeatureCollection",
      "option": type,
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": [

      ]
    }
    // json_point.features
    console.log(data)
    if (data.length > 0) {
      data.forEach(item => {
        json_point.features.push({
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [
              item.longitude,
              item.latitude
            ],
          },
          "properties": {
            "data": item,
            "type": 'cameraType',
            "id": item.id
          },
        })
      })
    }


    let thingmaps = document.getElementById('thingmap_zizhi')
    thingmaps.contentWindow.postMessage(json_point, "*")

  },
  ADD_MARKER(state, res) {
    let json_point = {
      "type": "FeatureCollection",
      "option": res.type,
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": []
    }
    console.log(res)
    if (res.data.length > 0) {
      res.data.forEach(item => {
        json_point.features.push(
          {
            "type": "Feature",
            "geometry": {
              "type": "Point",
              "coordinates": [
                item.lng,
                item.lat
              ],
            },
            "properties": {
              "data": item.events == null ? [] : item.events,
              "type": 'event',
              "id": item.events == null ? item.id : ''
            },
          }
        )
      })
      let thingmap = document.getElementById('thingmap')
      thingmap.contentWindow.postMessage(json_point, "*")
    }

  },
  ADD_TASK_MARKER(state, res) {
    console.log(res)
    let json_point = {
      "type": "FeatureCollection",
      "option": res.type,
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": []
    }
    res.data.forEach(item => {
      json_point.features.push(
        {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [
              item.lng,
              item.lat
            ],
          },
          "properties": {
            "NAME": item.address,
            "id": item.id,
            "type": 'task'
          },
        }
      )
    })
    let thingmap = document.getElementById('thingmap')
    thingmap.contentWindow.postMessage(json_point, "*")
  },
  ADD_TASK_HANDLE_MARKER(state, res) {
    let json_point = {
      "type": "FeatureCollection",
      "option": res.type,
      "crs": {
        "type": "name",
        "properties": {
          "name": "EPSG:4326"
        }
      },
      "features": []
    }
    res.data.forEach(item => {
      json_point.features.push(
        {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [
              item.lng,
              item.lat
            ],
          },
          "properties": {
            "NAME": item.address,
            "id": item.id,
            "type": 'task'
          },
        }
      )
    })
    let thingmap = document.getElementById('thingmap_zizhi')
    thingmap.contentWindow.postMessage(json_point, "*")
  },
  GET_CAMERAMARKER_INFO(state, res) {
    state.singlecameraInfo = res
  },
  GET_DIANTICAMERAMARKER_INFO(state, res) {
    state.singledianticameraInfo = res
  },
  CHANGE_SINGLEDIANTIVISIBLE(state, res) {
    console.log('====================================');
    console.log('打开电梯');
    console.log('====================================');
    state.singlediantivisible = res
  },
  CHANGE_SINGLEVISIBLE(state, res) {
    state.singlecameraVisible = res
  },
  SET_WAREHOUSE_ID(state, res) {
    state.wareHouseId = res
  },
};
export default {
  state,
  actions,
  mutations,
};
