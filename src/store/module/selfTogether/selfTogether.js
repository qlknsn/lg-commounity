import {
    getDistrictName,
    getEventLineInfo,
    getEventStatisticsVo,
    eventTypes,
    serviceDashboard,
    attendance,
    getMeetingList,
    getMeetingDetail,
    getTaskDetail,
    getMeetingTypeList,
    // getDistrictTree
} from "@/store/api/selfTogether/selfTogether";
import { getHkCameraByTag,getLatLng,getJMLatLng } from "@/store/api/lifeTizheng/lifeTizheng";

const state = {
    level: null,
    juweiNames: [],
    shequNames: [],
    warngingEventpopVisible: true,
    warngingEventpopsource: '',
    eventstatisticsVo: {},
    attendanceInfo: {},
    eventStatis: {},
    meetingList: {},
    meetingDetail: {},
    taskDetail: {},
    taskParams: {
        timeRangeType: 6
    },
    warningParams: {
        timeRangeType: 6,
        type: 2
    },
    warningParams: {
        timeRangeType: 6,
        type: 2
    },
    warningpopParams: {
        timeRangeType: 6,
        type: 1
    },
    qushiParams: {
        timeRangeType: 6,
        type: 'XFZD'
    },
    getMeetingTypeList: [],
    qushiParamsTimeObj: {}



}
const actions = {
    // 画区域
  getLatLng({ commit }, params) {
    let p = getLatLng(params)
    p.then(res => {

      let datacircle =  {
        "type": "FeatureCollection",
        "option":"circle",
        "features":res.data.features
      }
      let a =  res.data.features.filter(item=>{
        return item.properties.districtId ==localStorage.getItem('bigparentDistrictId')
      })
      let dataorientation ={
        "type": "FeatureCollection",
        "option":"orientation",
        "features":a
      } 
      let thingmap = document.getElementById('thingmap_zizhi')
      thingmap.contentWindow.postMessage(datacircle, "*")
      thingmap.contentWindow.postMessage(dataorientation, "*")
    })
  },
   // 获取建模数据
   getJMLatLng({ commit }, params) {
    let p = getJMLatLng(params)
    p.then(res => {
        let data ={
          "type": "FeatureCollection",
          "option":"JM",
          "features":res.data.features
        }
        let thingmap = document.getElementById('thingmap_zizhi')
        thingmap.contentWindow.postMessage(data, "*")
    })
  },
    getDistrictName({ commit }, params) {
        let p = getDistrictName(params)
        p.then(res => {
          commit('GETDISTRICTNAME', res);
        })
      },
  
    // 摄像头列表
    getHkCameraByTag_zhili({ commit }, params) {
        let par = { tag: params.tag }
        let p = getHkCameraByTag(par)
        p.then(res => {
            console.log(res)
            // commit('GET_CAMERAMARKER_LIST',res)
            commit('ADD_MARKER_LIST_ZHILI', { data: res.data, type: params.type })
        })
    },
    getMeetingTypeList({ commit }, params) {
        let p = getMeetingTypeList()
        p.then(res => {
            commit('MEETING_TYPE_LIST', res);
        })
    },
    attendance({ commit }, params) {
        let p = attendance(params)
        p.then(res => {
            commit('ATTENDANCE', res.data)
        })
    },
    serviceDashboard({ commit }, params) {
        let p = serviceDashboard(params)
        commit;
        return p
    },
    eventTypes({ commit }, params) {
        let p = eventTypes(params)
        commit;
        return p
    },
    getEventLineInfo({ commit }, params) {
        let p = getEventLineInfo(params)
        commit;
        return p
    },
    getEventStatisticsVo({ commit }, params) {
        let p = getEventStatisticsVo(params)
        p.then(res => {
            commit('GET_EVENTSTATISTICS_VO', res.data)
        })
    },
    getMeetingList({ commit }, params) {
        let p = getMeetingList(params)
        p.then(res => {
            commit('GET_MEETING_LIST', res)
        })
    },
    getMeetingDetail({ commit }, params) {
        let p = getMeetingDetail(params)
        p.then(res => {
            commit('GET_MEETING_DETAIL', res)
        })
    },
    getTaskDetailXue({ commit }, params) {
        let p = getTaskDetail(params)
        p.then(res => {
            commit('GET_TASK_DETAIL', res);
        })
    },
    // getDistrictTree({ commit }, params) {
    //     let p = getDistrictTree(params)
        // p.then(res => {
        //     commit('GET_TASK_DETAIL', res);
        // })
//         return p;
//     }
 }
const mutations = {
    SERVICE_PARAMS(state, res){
        console.log(res)
        state.serviceParams=res
    },
    GETDISTRICTNAME(state, res) {
        const level = res.data.map((item) => {
          return item.level
        })
        state.level = level[0]
    
        var arr1 = []
        for (var i of res.data) {
          arr1 = arr1.concat(i.children)
        }
        var arr2 = []
        for (var j of arr1) {
          arr2 = arr2.concat(j.children)
        }
        if (level[0] == 3) {//镇账号
          state.shequNames = arr1.map((item) => {
            return { label: item.id, value: item.name }
          })
          state.juweiNames = arr2
        } else if (level[0] == 4) {//社区账号
          state.juweiNames = arr1.map((item) => {
            return { label: item.id, value: item.name }
          })
        }
      },
    // 摄像机撒点
    ADD_MARKER_LIST_ZHILI(state, { data = [], type }) {

        let json_point = {
            "type": "FeatureCollection",
            "option": type,
            "crs": {
                "type": "name",
                "properties": {
                    "name": "EPSG:4326"
                }
            },
            "features": [

            ]
        }
        // json_point.features
        console.log(data)
        if (data.length > 0) {
            data.forEach(item => {
                json_point.features.push({
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [
                            item.lng,
                            item.lat
                        ],
                    },
                    "properties": {
                        "data": item,
                        "type": 'cameraType',
                        "id": item.cameraCode
                    },
                })
            })
        }

        let thingmap = document.getElementById('thingmap_zizhi')
        thingmap.contentWindow.postMessage(json_point, "*")

    },
    CHANGE_QUSHI_PARAMS(state, res) {
        state.qushiParams.type = res
    },
    CHANGE_QUSHI_TIME_PARAMS(state, res) {
        state.qushiParams.timeRangeType = res.label
        state.qushiParamsTimeObj = res
    },
    CHANGE_WARNINGPOP_PARAMS(state, res) {
        state.warningpopParams.timeRangeType = res
    },
    CHANGE_WARNING_PARAMS(state, res) {
        state.warningParams.timeRangeType = res
    },
    CHANGE_NOTICE_PARAMS(state, res) {
        state.taskParams.timeRangeType = res
        state.qushiParams.timeRangeType = res
    },
    ATTENDANCE(state, res) {
        let zhibanzhang = res.filter(item => item.attendance == '值班长')
        let zhibanyuan = res.filter(item => item.attendance == '值班员')

        let paichusuo = zhibanyuan.filter(item => item.duty == '派出所')
        let juwei = zhibanyuan.filter(item => item.duty == '居委' && item.name !== '俊佳1')
        let chengguan = zhibanyuan.filter(item => item.duty == '社区城管' && item.name !== '测试')
        let wuye = zhibanyuan.filter(item => item.duty == '物业' && item.name !== '刘东')
        let anbao = zhibanyuan.filter(item => item.duty == '安保')
        chengguan = chengguan.splice(0, 2)
        console.log(juwei)
        let data = {
            zhibanzhang,
            paichusuo,
            juwei,
            chengguan,
            wuye,
            anbao,
        }
        state.attendanceInfo = data
    },
    GET_EVENTSTATISTICS_VO(state, res) {
        state.eventstatisticsVo = res
    },
    SET_WARNING_EVENT_POP(state, res) {
        state.warngingEventpopVisible = res
    },
    GET_WARNING_EVENT_POP_SOURCE(state, res) {
        state.warngingEventpopsource = res
    },
    EVENT_STATIS(state, res) {
        state.eventStatis = res;
    },
    GET_MEETING_LIST(state, res) {
        state.meetingList = res.data;
    },
    GET_MEETING_DETAIL(state, res) {
        state.meetingDetail = res.data;
    },
    GET_TASK_DETAIL(state, res) {
        state.taskDetail = res.data;
    },
    MEETING_TYPE_LIST(state, res) {
        let arr = [];
        res.data.meetings.forEach(element => {
            let obj = {};
            obj.value = element.name
            obj.label = element.value
            arr.push(obj)
        });
        state.getMeetingTypeList = arr;
    },
}
export default {
    state, actions, mutations
}
