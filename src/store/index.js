import { createStore } from 'vuex'
import visible from "../store/visible.js"
import selfTogether from "@/store/module/selfTogether/selfTogether.js"
import lifeTizheng from "@/store/module/lifeTizheng/lifeTizheng.js"
import cameraLoop from "@/store/module/cameraLoop/cameraLoop.js"
import infoMessage from "./module/infoMessage/infoMessage";
export default createStore({
  state: {
    isShowWenDialog: true
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    visible,
    selfTogether,
    lifeTizheng,
    cameraLoop,
    infoMessage
  }
})
