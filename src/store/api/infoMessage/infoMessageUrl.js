// 通知提醒
const api = {
  // 登录
  login:'/lgbig/linggang/user/loginV3',
  // 发送消息
  sendMassage:'/lgbig/linggang/notice/addNotice',
  // 最近消息
  recentMassage:'/lgbig/linggang/notice/recent/service',
  // 人物组织树
  personList:'/lgbig/linggang/notice/tree',
  getMessageList: "/lgbig/linggang/notice/list",
  getSearchList: "/lgbig/linggang/event/globalSearch",
  getTaskDetail: "/lgbig/linggang/event/getTaskDetail",
  getTootipList:"/lgbig/linggang/noticeContent/list",
  getConfig: "/lgbig/linggang/user/config",//获取加载图片前缀
  getCameraList:'/lgbig/linggang/user/config',
  queryDistrictByParentId:"/lgbig/linggang/camera/queryDistrictByParentId",
  faceRecords:"/lgbig/linggang/camera/faceRecords",
  faceCamera:"/lgbig/linggang/camera/faceCamera",
  carCamera:"/lgbig/linggang/camera/carCamera",
  carRecords:"/lgbig/linggang/camera/carRecords",
  cameraNum:"/lgbig/linggang/camera/cameraNum",
  getHkCameraByTagConfig:"/lgbig/linggang/camera/getHkCameraByTag",

}
export default api;
