import api from "./infoMessageUrl";
import { axios } from "@/utils/request";
export function login(params) {
  return axios({
    url: api.login,
    method:'post',
    data: params
  });
}
//发送提醒
export function sendMassage(params) {
  return axios({
    url: api.sendMassage,
    data: params,
    method: "post"
  });
}
// 近期提醒
export function recentMassage(params) {
  return axios({
    url: api.recentMassage,
    params: params
  });
}
export function personList(params) {
  return axios({
    url: api.personList,
    params: params,
    method: "post"
  });
}
export function getMessageList(params) {
  return axios({
    url: api.getMessageList,
    data: params,
    method: "post"
  });
}
export function getSearchList(params) {
  return axios({
    url: api.getSearchList,
    data: params,
    method: "post"
  });
}
export function getTaskDetail(params) {
  return axios({
    url: api.getTaskDetail,
    params: params
  });
}
export function getTootipList(params) {
  return axios({
    url: api.getTootipList,
    data:params,
    method:"post"
  });
}
export function getConfig() {
  return axios({
    url: api.getConfig,
    method:"get"
  });
}
export function queryDistrictByParentId() {
  return axios({
    url: api.queryDistrictByParentId,
    method:"get"
  });
}
export function faceRecords(data) {
  return axios({
    url: api.faceRecords,
    method:"post",
    data:data
  });
}
export function carRecords(data) {
  return axios({
    url: api.carRecords,
    method:"post",
    data:data
  });
}
export function faceCamera(data) {
  return axios({
    url: api.faceCamera,
    method:"get",
    params:data
  });
}
export function carCamera(data) {
  return axios({
    url: api.carCamera,
    method:"get",
    params:data
  });
}
export function cameraNum() {
  return axios({
    url: api.cameraNum,
    method:"get",
    // params:data
  });
}
export function getHkCameraByTagConfig(data) {
  return axios({
    url: api.getHkCameraByTagConfig,
    method:"get",
    params:data
  });
}
