// 视频轮巡
const api = {
    // 获取视频列表接口
    cameraList:'/lgbig/linggang/camera/list',
    // 获取新视频列表接口
    cameraListV1:'/lgbig/linggang/camera/listV1',
    // 获取视频列表接口
    // cameraList:'/lgbig/linggang/camera/list',
    // 取流
    cameraPreviewURL:'/lgbig/linggang/camera/cameraPreviewURL',
    // cameraPreviewURL:'/lgbig/linggang/camera/cameraPreviewURLV2',
    //任务列表
    eventList:"/lgbig/linggang/event/list",
    //获取视频轮巡配置
    getLoopCfg:"/lgbig/linggang/camera/getLoopCfg",
    //视频预热接口
    getpreUrl:"/lgbig/linggang/camera/preUrl",
    //大华接口
    cameraPreviewURLV2:"/lgbig/linggang/camera/cameraPreviewURLV2",
}
export default api;