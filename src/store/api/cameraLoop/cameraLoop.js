import api from "./cameraLoopUrl";
import { axios } from "@/utils/request";
// 获取视频列表
export function cameraList(params) {
  return axios({
    url: api.cameraList,
    method:'post',
    data: params
  });
}
// 取流
export function cameraPreviewURL(params) {
  return axios({
    url: api.cameraPreviewURL,
    method:'post',
    data: params
  });
}
// 任务列表
export function eventList(params) {
  return axios({
    url: api.eventList,
    method:'post',
    data:params
  });
}
// 新视屏列表
export function cameraListV1(params) {
  return axios({
    url: api.cameraListV1,
    method:'post',
    data:params
  });
}
// 视频轮巡配置
export function getLoopCfg(params) {
  return axios({
    url: api.getLoopCfg,
    method:'get',
    params:params
  });
}
// 视频预热接口
export function getpreUrl(params) {
  return axios({
    url: api.getpreUrl,
    method:'get',
    params:params
  });
}
// 大华取流
export function cameraPreviewURLV2(params) {
  return axios({
    url: api.cameraPreviewURLV2,
    method:'get',
    params:params
  });
}
