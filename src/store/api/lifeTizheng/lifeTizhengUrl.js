// 生命体征
const api = {
    // 健康状态
    healthStatus:'/lgbig/linggang/event/healthStatus',
    // 高频事件、时间类别
    getEventCount:'/lgbig/linggang/life/getEventCount',
    // 疫情布控
    getEpidemicCount:'/lgbig/linggang/life/getEpidemicCount',
    //任务列表
    getEventList:'/lgbig/linggang/event/list',
    // 近期待办
    getNoticeLife:'/lgbig/linggang/life/getNoticeLife',
    // 进出统计
    getInOutCount:'/lgbig/linggang/life/getInOutCount',
    //人脸抓拍列表
    getCameraSnap:"/lgbig/linggang/camera/cameraSnap",
    //人脸对比图片列表
    getCompared:"/lgbig/linggang/camera/compared",
    // 事件撒点
    eventPage:'/lgbig/linggang/event/page',
    // 消防站列表
    getFireHouse:'/lgbig/linggang/fireHouse/getFireHouse',
    // 电梯列表
    getElevator:'/lgbig/linggang/backend/getElevator',
    // 仓库列表
    getWarehouse:'/lgbig/linggang/backend/getWarehouse',
    // 视频列表
    getcamerabzList:'/lgbig/linggang/camera/list',
    // 时间列表
    getcamerabzList:'/lgbig/linggang/event/list',
    // 居委人，老人
    lifeUsers:'/lgbig/linggang/life/users',
    // 统计
    getInOutCountDashboard:'/lgbig/linggang/life/getInOutCount/Dashboard',
    // 疫情布控列表
    lifeList:'/lgbig/linggang/life/list',
    // 摄像头列表
    getHkCameraByTag:'/lgbig/linggang/camera/getHkCameraByTag',
    getRenovationList:'/lgbig/linggang/backend/getRenovationList',   //装修管理列表
    getLatLng:'/lgbig/linggang/life/getLatLng',   //区域
    // 获取模型数据
    getJMLatLng:'/lgbig/linggang/life/getJMLatLng', 
     //获取社区居委名称
     getDistrictName:"/lgbig/linggang/backend/districtTree",
     //楼栋详情
     getBuilding:"/lgbig/linggang/life/getBuilding",
     //房间详情
     getRoom:"/lgbig/linggang/life/getRoom",
     //电梯撒点
     getLift:"/lgbig/linggang/life/getLift",
     //仓库撒点
     getWarehouseList:"/lgbig/linggang/backend/getWarehouseList",
     //仓库详情
     getWareInfo:"/lgbig/linggang/life/getWarehouseDevice",
}
export default api;
