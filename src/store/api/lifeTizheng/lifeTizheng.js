import api from "./lifeTizhengUrl";
import { axios } from "@/utils/request";
// 登录
export function login(params) {
  return axios({
    url: api.login,
    method: 'post',
    data: params
  });
}

// 健康状态
export function healthStatus(params) {
  return axios({
    url: api.healthStatus,
    params: params
  });
}
// 高频事件、事件类别
export function getEventCount(params) {
  return axios({
    url: api.getEventCount,
    params: params
  });
}
// 疫情布控
export function getEpidemicCount(params) {
  return axios({
    url: api.getEpidemicCount,
    params: params
  });
}
// 任务列表
export function getEventList(params) {
  return axios({
    url: api.getEventList,
    data: params,
    method: "post"
  })
}
// 近期待办
export function getNoticeLife(params) {
  return axios({
    url: api.getNoticeLife,
    params: params
  });
}
// 进出统计
export function getInOutCount(params) {
  return axios({
    url: api.getInOutCount,
    params: params
  });
}
// 人脸砖拍列表
export function getCameraSnap(params) {
  return axios({
    url: api.getCameraSnap,
    method: 'get',
    params:params
  })
}
// 人脸对比列表
export function getCompared() {
  return axios({
    url: api.getCompared,
    method: 'get'
  })
}
// 事件撒点
export function eventPage(params) {
  return axios({
    url: api.eventPage,
    data: params,
    method: "post"
  });
}
// 微型消防站列表
export function getFireHouse(params) {
  return axios({
    url: api.getFireHouse,
    data: params,
    method: "post"
  });
}
// 微型消防站列表
export function getElevator(params) {
  return axios({
    url: api.getElevator,
    data: params,
    method: "post"
  });
}
// 仓库列表
export function getWarehouse(params) {
  return axios({
    url: api.getWarehouse,
    data: params,
    method: "post"
  });
}
// 视频保障
export function getcamerabzList(params) {
  return axios({
    url: api.getcamerabzList,
    data: params,
    method: "post"
  });
}
// 居委人、老人
export function lifeUsers(params) {
  return axios({
    url: api.lifeUsers,
    data: params,
    method: "post"
  });
}
// 进出统计
export function getInOutCountDashboard(params) {
  return axios({
    url: api.getInOutCountDashboard,
    params: params
  });
}
// 进出统计
export function lifeList(params) {
  return axios({
    url: api.lifeList,
    data: params,
    method:'post'
  });
}
// 摄像头列表
export function getHkCameraByTag(params) {
  return axios({
    url: api.getHkCameraByTag,
    
    params: params,
    method:'get'
  });
}
// 摄像头列表
export function getRenovationList(params) {
  return axios({
    url: api.getRenovationList,
    data: params,
    method:'post'
  });
}
// 区域
export function getLatLng(params) {
  return axios({
    url: api.getLatLng,
    params: params,
  });
}
// 获取建模数据
export function getJMLatLng(params) {
  return axios({
    url: api.getJMLatLng,
    params: params,
  });
}
  //获取社区居委名称
  export function getDistrictName(params) {
    console.log(params)
    return axios({
      url: api.getDistrictName+ `?parentDistrictId=${params}`,
      method:'get'
    });
  }
  //获取楼栋详情
  export function getBuilding(params) {
    return axios({
      url: api.getBuilding,
      params: params,
    });
  }
  //获取房间详情
  export function getRoom(params) {
    return axios({
      url: api.getRoom,
      params: params,
    });
  }
  //获取房间详情
  export function getLift(params) {
    return axios({
      url: api.getLift,
      data: params,
      method:"post"
    });
  }
  //获取仓库列表
  export function getWarehouseList(params) {
    return axios({
      url: api.getWarehouseList,
      data: params,
      method:"post"
    });
  }
  //获取仓库详情
  export function getWareInfo(params) {
    return axios({
      url: api.getWareInfo,
      data: params,
      method:"POST"
    });
  }
