import api from "./selfTogetherUrl";
import { axios } from "@/utils/request";

// 趋势看板
export function getEventLineInfo(params) {
  return axios({
    url: api.getEventLineInfo,
    method: 'get',
    params: params
  });
}
// 任务处置统计
export function getEventStatisticsVo(params) {
  return axios({
    url: api.getEventStatisticsVo,
    method: 'get',
    params: params
  });
}
// 事件类型
export function eventTypes(params) {
  return axios({
    url: api.eventTypes,
    method: 'get',
    params: params
  });
}
// 服务人员动态详情
export function serviceDashboard(params) {
  return axios({
    url: api.serviceDashboard,
    method: 'get',
    params: params
  });
}
// 管理力量
export function attendance(params) {
  return axios({
    url: api.attendance,
    method: 'get',
    params: params
  });
}
// 三会列表
export function getMeetingList(params) {
  return axios({
    url: api.getMeetingList,
    method: 'post',
    data: params
  });
}
// 三会详情
export function getMeetingDetail(params) {
  return axios({
    url: api.getMeetingDetail + params.meetingId,
    method: 'get',

  });
}
// 任务详情
export function getTaskDetail(params) {
  return axios({
    url: api.getTaskDetail,
    method: 'get',
    params: params
  });
}
// 三会筛选下拉框类型
export function getMeetingTypeList() {
  return axios({
    url: api.getMeetingTypeList,
    method: 'get',
  });
}
// 地区下拉框
// export function getDistrictTree(params) {
//   return axios({
//     url: api.getDistrictTree,
//     params:params
//   });
//获取社区居委名称
export function getDistrictName(params) {
  console.log(params)
  return axios({
    url: api.getDistrictName + `?parentDistrictId=${params}`,
    method: 'get'
  });
}
