// 自治共治
const api = {
    // 趋势看板
    getEventLineInfo: '/lgbig/linggang/self/getEventLineInfo',
    // 任务处置统计
    getEventStatisticsVo: '/lgbig/linggang/self/getEventStatisticsVo',
    // 事件类型
    eventTypes: '/lgbig/linggang/event/eventTypes',
    // 服务人员动态详情
    serviceDashboard: '/lgbig/linggang/self/serviceDashboard',
    // 管理力量
    attendance: '/lgbig/linggang/self/attendance',
    //三会列表
    getMeetingList: '/lgbig/linggang/self/page',
    //三会详情
    getMeetingDetail: "/lgbig/linggang/self/info/",
    //任务详情
    getTaskDetail: "/lgbig/linggang/event/getTaskDetail",
    //三会类型筛选下拉框
    getMeetingTypeList: "/lgbig/linggang/backend/dropdown",
    // 地区下拉框
    // getDistrictTree:"/lgbig/linggang/backend/districtTree",
    //获取社区居委名称
    getDistrictName: "/lgbig/linggang/backend/districtTree"
}
export default api;