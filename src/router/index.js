import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/dashboard.vue";
import Login from "../views/login.vue";
import Camerayure from "../components/cameraLoop/cameraYure.vue";
import cameraDahua from "../components/cameraLoop/cameraboxclient.vue";


const routes = [
  {
    path: "/screen",
    name: "Home",
    component: Home,
    meta: { title: "社区管理与服务平台" },
  },
  {
    path: "/",
    name: "Login",
    component: Login,
    meta: { title: "社区管理与服务平台-登录" },
  },
  {
    path: "/camerayure",
    name: "camerayure",
    component: Camerayure,
    meta: { title: "视频预热" },
  },
  {
    path: "/cameraDahua",
    name: "cameraDahua",
    component: cameraDahua,
    meta: { title: "视频预热" },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});
router.beforeEach((to, from, next) => {
  window.document.title = to.meta.title || "";
  next();
});
export default router;
