import { createApp } from "vue";
import App from "./App.vue";
import { Base64 } from "js-base64";
import router from "./router";
import store from "./store";
import ElementPlus from "element-plus";
import "swiper/dist/css/swiper.css";
import "element-plus/dist/index.css";
import zhCn from "element-plus/es/locale/lang/zh-cn";
import VueAwesomeSwiper from "vue-awesome-swiper";
import { loadScript } from "esri-loader";
import 'video.js/dist/video-js.css'

// const options = {
//   url: "http://10.89.5.14/api/library/4.16/init.js",
//   // url: require("@/components/lifeTizheng/arcgis/init.js"),
// };
// loadScript(options);

createApp(App)
  .use(store)
  .use(router)
  .use(Base64)
  .use(ElementPlus, { locale: zhCn })
  .use(VueAwesomeSwiper)
  .mount("#app");
