# 临港社区综合管理平台

大屏端，包括 16:9 与 32:9 两个版本


—————————————————————————————————————————————
### 项目名称
- 社区管理与服务平台 - 临港家园居委

### 项目简介
- 该平台服务与临港各社区实现自主发现案件、案件上报、案件处理，以及社区基础数据汇总，视频调阅，社区发布通知等功能

### 技术架构
- 前端：大屏采用vue3框架搭建，视频播放采用websocket推流实现，后台管理vue全家桶实现，处置端使用微信小程序开发
- 服务端：java
  
### 项目主要结构
- view视图主文件 `/src/view`
- view视图组件 `src/components`
- 路由模块  `/src/router`<br>
  所有路由跳转都在 `/src/router/index.js` 文件内
<br>

- vuex模块
##### 
  1. `生命体征模块`:  <br>
        接口地址文件：`/src/store/api/lifeTizheng/lifeTizhengUrl.js`<br>
        ajax方法定义：`/src/store/api/lifeTizheng/lifeTizheng.js`<br>
        ajax方法请求：`/src/store/module/lifeTizheng/lifeTizheng.js`<br>
##### 
  2. `自治共治模块`: <br>
        接口地址文件：`/src/store/api/selfTogether/selfTogetherUrl.js`<br>
        ajax方法定义：`/src/store/api/selfTogether/selfTogether.js`<br>
        ajax方法请求：`/src/store/module/selfTogether/selfTogether.js`<br>
##### 
  3. `视频轮巡模块`:<br> 
        接口地址文件：`/src/store/api/cameraLoop/cameraLoopUrl.js`<br>
        ajax方法定义：`/src/store/api/cameraLoop/cameraLoop.js`<br>
        ajax方法请求：`/src/store/module/cameraLoop/cameraLoop.js`<br>
#####        
  4. `通知提醒模块`: <br>
        接口地址文件：`/src/store/api/notice/noticeUrl.js`<br>
        ajax方法定义：`/src/store/api/notice/notice.js`<br>
        ajax方法请求：`/src/store/module/notice/notice.js`

### 开发环境配置及部署步骤（测试环境，正式环境）

- 项目下载到本地：`git clone http://gitlab.bearhunting.cn/frontend/lg-community.git`
- 安装依赖：`cd lg-community` 、 `npm install`
- 运行项目： `npm run serve`
- 打包项目：`npm run build`
- 压缩文件：`tar zcvf lgdp lgdp.tar.gz`
- 上传至服务器指定目录 `cd /export/apps/....`  `rz` 选择 `lgdp.tar.gz`
- 解压文件：`tar zxvf lgdp.tar.gz`

 <br>
 部署才用的是飞流自动化部署，详情请联系运维，解释权归运维所有 @朱大维 
  
##### 测试环境nginx反向代理配置
- 192.168.199.169

- .conf
  ```js
    
    location /lgbig {
        proxy_pass: http://192.168.63.242:39001;
    }
  ```

##### 正式环境nginx反向代理配置
- 10.242.212.153
- .conf
  ```js
    location /lgbig {
        proxy_pass: http://10.242.212.153:4000;
    }
  ```
### 测试环境地址(?v=1为32:9版本)
- http://frontend.bearhunting.cn:57001/
- 用户名：18888888888
- 密码：7ujmBGT&
### 正式环境地址(?v=1为32:9版本)
- http://10.242.212.153:61617/
- 用户名：18888888888
- 密码：7ujmBGT&
  
### 项目仓库说明
- git仓库：http://gitlab.bearhunting.cn/frontend/lg-community.git
- 分支：
  1. `master`: *主分支，项目部署所用分支，所又的子分支最终 merge 到 master该分支不可修改，所以克隆项目之后及时创建自己的分支，在自己的分支修改*
  2. `tolerious`：*冯赫龙的分支*
  3. `qlk`：*钱立昆的分支*
  4. `xueshuai`：*薛帅帅的分支*
  5. `wenbo`：*黄文博的分支*（$\color{FF0000}{已离职}$）